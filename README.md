# Conectando Tecnologia e Conhecimento: Minha Jornada no Mundo do Software Livre

Repositório para armazenar os Slides da palestra **Conectando Tecnologia e Conhecimento: Minha Jornada no Mundo do Software Livre** ministrado por:

- [Samuel Gonçalves](https://beacons.ai/sgoncalves)

No evento JoinCommunity no dia 23 de setembro de 2023.

## whoami

# 🇧🇷 Samuel Gonçalves 🐧

* Tech Lead na 4Linux
* Consultor de Tecnologia nas áreas **Segurança Ofensiva** e **DevSecOps**
* Mais de 10 anos de experiência 💻
* *"6k++"* alunos ensinados no 🌎
* Músico, Contista, YouTuber e Podcaster
* "Evangelista" do Software Livre 🐧

> Contato: [https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)

## Objetivos da Palestra

- Compartilhar minha jornada pessoal e profissional no mundo do Software Livre;
- Discutir a importância, benefícios e desafios do Software Livre;
- Explorar oportunidades de carreira e educação na área;
- Orientar iniciantes sobre como se envolver com a comunidade e tecnologias Open Source.