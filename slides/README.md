---
marp: true
theme: gaia
class: 
  - lead
  - invert
paginate: true
backgroundImage: url(images/sec-bg.svg)
style: |
  :root {
    --color-background: #263238;
    --color-foreground: #EEEEEE;
    --color-highlight: #FF6F00;
    --color-dimmed: #9E9E9E;
  }
  h1 {
    text-shadow: 1px 1px 15px #000;
    --color-highlight: #f6f6f6;
  }
  section::after {
    font-weight: bold;
    content: attr(data-marpit-pagination) '/' attr(data-marpit-pagination-total);
    font-size: 11pt;
    color: #fff;
    text-shadow: 1px 1px 0 #000;
  }

---
<!-- _backgroundImage: url('images/main-bg.svg
') -->
<!-- _paginate: false -->
# **Conectando Tecnologia e Conhecimento**
#
## Minha Jornada no Mundo do Software Livre
###
*Samuel Gonçalves*

---
> ##  *Num mundo inundado de informações irrelevantes, clareza é poder*

*Yuval Harari*

---
<style scoped>
  p {
    font-size: 13pt;
  }
  {
   font-size: 28px;
  }
  img[alt~="center"] {
    display: block;
    margin: 0 auto;
  }
</style>

<!-- _paginate: false -->

![bg right:40%](images/perfil.png)

# 🇧🇷 Samuel Gonçalves 🐧

* Tech Lead na 4Linux
* Consultor de Tecnologia nas áreas **Segurança Ofensiva** e **DevSecOps**
* Mais de 10 anos de experiência 💻
* *"6k++"* alunos ensinados no7 🌎
* Músico, Contista, YouTuber e Podcaster
* "Evangelista" do Software Livre 🐧

---
## Entre em contato
[https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)
![bg left:50% 70%](images/qrcode-contato-white.png)

---
<style scoped>
  h4{
    font-size: 13pt;
    font-weight: normal;
  }
</style>

![bg right:40% 80%](images/qrcode-slides.jpg)
## Esses slides são OpenSource! 
Feitos com ❤️ pra você!

---
<style scoped>
  h4{
    font-size: 13pt;
    font-weight: normal;
  }
</style>

![bg right:40% 80%](images/qrcode-slides.jpg)
## Esses slides são OpenSource! 
Feitos com ❤️ pra você!

#### *(sacanagem, usei markdown mesmo...kkk)*


---
## Objetivos da Palestra

- Compartilhar minha jornada pessoal e profissional no mundo do Software Livre;
- Discutir a importância, benefícios e desafios do Software Livre;
- Explorar oportunidades de carreira e educação na área;
- Orientar iniciantes sobre como se envolver com a comunidade e tecnologias Open Source.

---
## **Você se lembra o que estava fazendo em 2010?**

---
## Acontecimentos importantes de 2010!

- Orkut: A rede só foi descontinuada em 2014
- Computação em nuvem: Ano da 'consolidação' da tecnologia;
- Tablets: Lançamento do iPad e tablets na moda, com grandes promessas;
- 4G: O 4G surge e começa a ser implementado;
- WhatsApp: Criado em 2009, começa a tomar corpo.

---
<style scoped>
  p{
    font-size: 14pt;
    font-weight: normal;
  }
</style>
![bg](images/ubuntu10.jpg)
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
###### Meu primeiro contato com linux!
*Obs.: Este é um ubuntu 10.04!*

---
## Primeiro emprego

Me tornei "**Auxiliar de Informática**" na escola em que cursava o ensino médio. 

---
## **Seguindo adiante...**

Em 2012 começo a cursar "Sistemas de Informação" na UEG

---
## **Por que escolhi o Software Livre?**
- Comunidade!
- Motivações pessoais
- Primeiro contato e impressões
- Valores alinhados
- **Software do meu jeito!!**

---
> Liberdade implica **responsabilidade**, é por isso que tanta gente tem medo dela

*George Bernard Shaw*

*Indicação: [Boa noite Internet - A culpa não é minha](https://www.boanoiteinternet.com.br/2022/10/09/a-culpa-nao-e-minha/)*

---

## Responsabilidade

Muitas vezes usar software livre é chamar a responsabilidade pra si. Isso tem dois lados, o bom e o ruim!

---

## A resposta é sempre **DEPENDE**

---

## Liberdade passa pelo conhecimento!

É preciso **aprender de verdade** como a tecnologia funciona para *se dar bem* com software livre.

*Indicação: [Fabio Akita - Aprendendo a aprender](https://youtu.be/oUPaJxk6TZ0?si=gIgeM7p4yrStuc1d)*

---
> Linux Is User Friendly...
> Just Very Picky Who It's Friends Are!

*Autor desconhecido (por mim)*

---

## Show! Mas, como eu ganho **dinheiro?**

* Software "Livre" refere-se à liberdade, não à ausência de custo.
* É possível vender cópias e oferecer suporte ou garantia como serviços pagos.
* Segundo a **RedHat** 90% das empresas líderes de TI usam soluções open source empresariais

*Indicação: [IBM, Red Hat and Free Software: An old maddog’s view](https://www.lpi.org/blog/2023/07/30/ibm-red-hat-and-free-software-an-old-maddogs-view/)*

---
## Busca por profissionais "OpenSource"

![](images/vagas-devops-eu.png)

---

![](images/vagas-devops-eua.png)

---

![bg : 80%](images/vagas-linux-br.png)

---

![bg:100%](images/vagas-software-livre-br.png)

---
## Colabore!

A cultura do OpenSource permite que equipes colaborem mais entre si, tornando o desenvolvimento mais ágil e seguro

*Ps.: Isso é muito **DevOps***

---

## **E o conhecimento?**
- Cursos de:
    * Linux
    * DevOps
    * DevSecOps
    * Segurança Ofensiva
    * Observabilidade
    * Containers (Docker e Kubernetes)

---

## **A importância da Comunidade**
- A força da colaboração

---

## **Se envolva!**
- Participe de uma comunidade!

---

## **Conclusão**
- Reflexão sobre a jornada
- O futuro do software livre e seu papel

---
<style scoped>
  h4{
    font-size: 13pt;
    font-weight: normal;
  }
</style>

![bg right:40% 80%](images/qrcode-slides.jpg)
## Esses slides são OpenSource! 
Se  não baixou, corre!

---

## CURSO GRÁTIS!

![bg left:50% 70%](images/qrcode-contato-white.png)

---

## **Perguntas!**

---
## Obrigado!

![bg right:50% 70%](images/qrcode-contato-white.png) 
Vamos manter contato!